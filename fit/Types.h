#pragma once

#include <functional>
#include <utility>
#include <vector>

#include "VectorConfiguration.h"
#include "MemStore.h"
#include "MemIterator.h"
#include "MemView.h"

// We need the Gaudi types FitNode, renamed to avoid collisions
#include "../gaudi/Types.h"

typedef Node NodeAOS;
typedef FitNode FitNodeAOS;

// Names for templates
namespace VectorFit {

static constexpr std::array<PRECISION, 15> m_initialCovarianceValues {{400, 0, 400, 0, 0, 0.01, 0, 0, 0, 0.01, 0, 0, 0, 0, 0.0001}};

static _type_aligned std::array<PRECISION, 15> m_initialScalarCovariance = m_initialCovarianceValues;

struct Node {
  Type m_type;

  Node () = default;
  Node (const Node& node) = default;
  Node (const FitNodeAOS& node) {
    m_type = node.m_type;
  }
};

struct FitNode : public Node {
  unsigned m_index;
  Mem::View::State m_forwardState;
  Mem::View::State m_backwardState;
  Mem::View::TrackMatrix<25> m_forwardTransportMatrix;
  Mem::View::TrackMatrix<25> m_backwardTransportMatrix;
  Mem::View::SmoothState m_smoothState;
  Mem::View::NodeParameters m_nodeParameters;

  FitNode () = default;
  FitNode (const FitNode& node) = default;
  FitNode (const FitNodeAOS& node)
  : Node(node)
    {}

  FitNode (const FitNodeAOS& node, const unsigned& index)
  : Node(node),
    m_index(index) 
    {}

  operator FitNodeAOS () const {
    FitNodeAOS n;
    n.m_type = m_type;

    if (m_nodeParameters.m_basePointer != nullptr) {
      n.m_projectionMatrix = m_nodeParameters.m_projectionMatrix.operator TrackVectorAOS();
      n.m_refVector = m_nodeParameters.m_referenceVector.operator StateVector();
      n.m_transportVector = m_nodeParameters.m_transportVector.operator TrackVectorAOS();
      n.m_noiseMatrix = m_nodeParameters.m_noiseMatrix.operator TrackSymMatrixAOS();
      n.m_errMeasure = *(m_nodeParameters.m_errorMeasure);
      n.m_refResidual = *(m_nodeParameters.m_referenceResidual);
    }

    if (m_forwardState.m_basePointer != nullptr) {
      n.m_filteredState[Forward].m_stateVector = m_forwardState.m_updatedState.operator TrackVectorAOS();
      n.m_filteredState[Forward].m_covariance = m_forwardState.m_updatedCovariance.operator TrackSymMatrixAOS();
    }

    if (m_backwardState.m_basePointer != nullptr) {
      n.m_filteredState[Backward].m_stateVector = m_backwardState.m_updatedState.operator TrackVectorAOS();
      n.m_filteredState[Backward].m_covariance = m_backwardState.m_updatedCovariance.operator TrackSymMatrixAOS();
    }

    if (m_smoothState.m_basePointer != nullptr) {
      n.m_state.m_stateVector = m_smoothState.m_state.operator TrackVectorAOS();
      n.m_state.m_covariance = m_smoothState.m_covariance.operator TrackSymMatrixAOS();
      n.m_residual = *(m_smoothState.m_residual);
      n.m_errResidual = *(m_smoothState.m_errResidual);
    }

    if (m_forwardTransportMatrix.m_basePointer != nullptr) {
      n.m_transportMatrix = m_forwardTransportMatrix.operator TrackMatrixAOS();
    }

    if (m_backwardTransportMatrix.m_basePointer != nullptr) {
      n.m_invertTransportMatrix = m_backwardTransportMatrix.operator TrackMatrixAOS();
    }

    return n;
  }

  // The class U type specifier is conditional to S
  // One could do the same with a partially specialized class,
  // but here we want a function
  template<class R, class S,
    class U = 
      std::conditional_t<std::is_same<S, Op::Covariance>::value, Mem::View::TrackSymMatrix,
      std::conditional_t<std::is_same<S, Op::NoiseMatrix>::value, Mem::View::TrackSymMatrix,
      std::conditional_t<std::is_same<S, Op::StateVector>::value, Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::ReferenceVector>::value, Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::TransportVector>::value, Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::TransportMatrix>::value, Mem::View::TrackMatrix<25>, PRECISION>>>>>>>
  >
  inline const U& get () const;

  template<class R, class S,
    class U =
      std::conditional_t<std::is_same<S, Op::Covariance>::value,       Mem::View::TrackSymMatrix, 
      std::conditional_t<std::is_same<S, Op::NoiseMatrix>::value,      Mem::View::TrackSymMatrix,
      std::conditional_t<std::is_same<S, Op::StateVector>::value,      Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::ReferenceVector>::value,  Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::ProjectionMatrix>::value, Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::TransportVector>::value,  Mem::View::TrackVector,
      std::conditional_t<std::is_same<S, Op::TransportMatrix>::value,  Mem::View::TrackMatrix<25>, PRECISION>>>>>>>
  >
  inline U& get ();
};

#include "NodeGetters.h"

struct Track {
  std::vector<VectorFit::FitNode> m_nodes;

  // Active measurements from this node
  unsigned m_forwardUpstream;
  unsigned m_backwardUpstream;

  // Chi square of track
  PRECISION m_forwardFitChi2 = 0.0;
  PRECISION m_backwardFitChi2 = 0.0;
  
  // Some other things
  unsigned m_index;
  int m_ndof = 0;
  int m_parent_nTrackParameters;

  // Initial covariances
  TrackSymMatrixContiguous m_initialForwardCovariance;
  TrackSymMatrixContiguous m_initialBackwardCovariance;

  Track () = default;
  Track (const Track& track) = default;
  Track (
    const std::vector<FitNodeAOS>& track,
    const unsigned& trackNumber,
    Mem::Store& store
  ) : m_index(trackNumber) {
    // Generate the nodes
    int i=0;
    for (const auto& node : track) {
      m_nodes.push_back(VectorFit::FitNode(node, i++));
    }

    // Copy the initial forward predicted and backward predicted covariance
    m_initialForwardCovariance  = track.front().m_predictedState[0].m_covariance;
    m_initialBackwardCovariance = track.back().m_predictedState[1].m_covariance;

    // Copy the initial m_parent_trackParameters
    m_parent_nTrackParameters = track.front().m_parent_nTrackParameters;

    // Copy all other state and covariances (to avoid 0x0 accesses - this shouldn't be in Gaudi)
    for (unsigned i=0; i<m_nodes.size(); ++i) {
      const FitNodeAOS& oldnode = track[i];
      FitNode& node = m_nodes[i];

      node.m_forwardState.setBasePointer(VectorFit::Mem::View::State(store.getNextElement()));
      node.m_backwardState.setBasePointer(VectorFit::Mem::View::State(store.getNextElement()));

      node.get<Op::Forward, Op::StateVector>().copy(oldnode.m_filteredState[0].m_stateVector);
      node.get<Op::Forward, Op::Covariance>().copy(oldnode.m_filteredState[0].m_covariance);
      node.get<Op::Backward, Op::StateVector>().copy(oldnode.m_filteredState[1].m_stateVector);
      node.get<Op::Backward, Op::Covariance>().copy(oldnode.m_filteredState[1].m_covariance);
    }

    m_forwardUpstream = 0;
    m_backwardUpstream = track.size() - 1;

    bool foundForward = false;
    bool foundBackward = false;
    for (unsigned i=0; i<track.size(); ++i) {
      const int reverse_i = track.size() - i - 1;

      if (!foundForward && track[i].m_type == HitOnTrack) {
        foundForward = true;
        m_forwardUpstream = i;
      }

      if (!foundBackward && track[reverse_i].m_type == HitOnTrack) {
        foundBackward = true;
        m_backwardUpstream = i;
      }
    }
  }
};

}
