#pragma once

#include "../Types.h"
#include "Math.h"

namespace VectorFit {

namespace Scalar {

template<bool F, bool B>
inline void smoother (
  FitNode& node
);

template<>
inline void smoother<false, true> (
  FitNode& node
) {
  node.get<Op::Smooth, Op::StateVector>().setBasePointer(node.get<Op::Backward, Op::StateVector>());
  node.get<Op::Smooth, Op::Covariance>().setBasePointer(node.get<Op::Backward, Op::Covariance>());
}

template<>
inline void smoother<true, false> (
  FitNode& node
) {
  node.get<Op::Smooth, Op::StateVector>().setBasePointer(node.get<Op::Forward, Op::StateVector>());
  node.get<Op::Smooth, Op::Covariance>().setBasePointer(node.get<Op::Forward, Op::Covariance>());
}

template<>
inline void smoother<true, true> (
  FitNode& node
) {
  Math::average (
    node.get<Op::Forward, Op::StateVector>(),
    node.get<Op::Forward, Op::Covariance>(),
    node.get<Op::Backward, Op::StateVector>(),
    node.get<Op::Backward, Op::Covariance>(),
    node.get<Op::Smooth, Op::StateVector>(),
    node.get<Op::Smooth, Op::Covariance>()
  );
}

inline void updateResiduals (
  FitNode& node
) {
  PRECISION value {0.0}, error {0.0};
  if (node.m_type == HitOnTrack) {
    const TrackVector& H = node.get<Op::NodeParameters, Op::ProjectionMatrix>();
    PRECISION HCH;
    Math::similarity_5_1 (
      node.get<Op::Smooth, Op::Covariance>(),
      H,
      &HCH);
    
    // H * (refx - T)
    auto mulSubMatrix = [] (const TrackVector& a, const TrackVector& b, const TrackVector& c) {
      return (a[0] * (b[0] - c[0]) +
              a[1] * (b[1] - c[1]) +
              a[2] * (b[2] - c[2]) +
              a[3] * (b[3] - c[3]) +
              a[4] * (b[4] - c[4]));
    };

    const TrackVector& refX = node.get<Op::NodeParameters, Op::ReferenceVector>();
    const PRECISION V = node.get<Op::NodeParameters, Op::ErrMeasure>() * node.get<Op::NodeParameters, Op::ErrMeasure>();
    const PRECISION sign = (node.m_type == HitOnTrack) ? -1 : 1;
    value = node.get<Op::NodeParameters, Op::ReferenceResidual>() + mulSubMatrix(H, refX, node.get<Op::Smooth, Op::StateVector>());
    error = V + sign * HCH;
  }
  *node.m_smoothState.m_residual = value;
  *node.m_smoothState.m_errResidual = error;
}

}

}
