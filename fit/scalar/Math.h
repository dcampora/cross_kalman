#pragma once

#include "../Types.h"

namespace VectorFit {

namespace Scalar {

struct Math {
  static inline void similarity_5_1 (
    const TrackSymMatrix& Ci,
    const TrackVector& Fi,
    PRECISION* ti
  ) {
    auto _0 = Ci[ 0]*Fi[0]+Ci[ 1]*Fi[1]+Ci[ 3]*Fi[2]+Ci[ 6]*Fi[3]+Ci[10]*Fi[4];
    auto _1 = Ci[ 1]*Fi[0]+Ci[ 2]*Fi[1]+Ci[ 4]*Fi[2]+Ci[ 7]*Fi[3]+Ci[11]*Fi[4];
    auto _2 = Ci[ 3]*Fi[0]+Ci[ 4]*Fi[1]+Ci[ 5]*Fi[2]+Ci[ 8]*Fi[3]+Ci[12]*Fi[4];
    auto _3 = Ci[ 6]*Fi[0]+Ci[ 7]*Fi[1]+Ci[ 8]*Fi[2]+Ci[ 9]*Fi[3]+Ci[13]*Fi[4];
    auto _4 = Ci[10]*Fi[0]+Ci[11]*Fi[1]+Ci[12]*Fi[2]+Ci[13]*Fi[3]+Ci[14]*Fi[4];
    *ti = Fi[ 0]*_0 + Fi[ 1]*_1 + Fi[ 2]*_2 + Fi[ 3]*_3 + Fi[ 4]*_4;
  }

  template<class T>
  static inline void similarity_5_5 (
    const TrackMatrix<25>& tm,
    const T& uc,
    TrackSymMatrix& pc
  ) {
    auto _0 = uc[ 0]*tm[0]+uc[ 1]*tm[1]+uc[ 3]*tm[2]+uc[ 6]*tm[3]+uc[10]*tm[4];
    auto _1 = uc[ 1]*tm[0]+uc[ 2]*tm[1]+uc[ 4]*tm[2]+uc[ 7]*tm[3]+uc[11]*tm[4];
    auto _2 = uc[ 3]*tm[0]+uc[ 4]*tm[1]+uc[ 5]*tm[2]+uc[ 8]*tm[3]+uc[12]*tm[4];
    auto _3 = uc[ 6]*tm[0]+uc[ 7]*tm[1]+uc[ 8]*tm[2]+uc[ 9]*tm[3]+uc[13]*tm[4];
    auto _4 = uc[10]*tm[0]+uc[11]*tm[1]+uc[12]*tm[2]+uc[13]*tm[3]+uc[14]*tm[4];
    pc[ 0] = tm[ 0]*_0 + tm[ 1]*_1 + tm[ 2]*_2 + tm[ 3]*_3 + tm[ 4]*_4;
    pc[ 1] = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
    pc[ 3] = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
    pc[ 6] = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
    pc[10] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4; 
    _0 =  uc[ 0]*tm[5]+uc[ 1]*tm[6]+uc[ 3]*tm[7]+uc[ 6]*tm[8]+uc[10]*tm[9];
    _1 =  uc[ 1]*tm[5]+uc[ 2]*tm[6]+uc[ 4]*tm[7]+uc[ 7]*tm[8]+uc[11]*tm[9];
    _2 =  uc[ 3]*tm[5]+uc[ 4]*tm[6]+uc[ 5]*tm[7]+uc[ 8]*tm[8]+uc[12]*tm[9];
    _3 =  uc[ 6]*tm[5]+uc[ 7]*tm[6]+uc[ 8]*tm[7]+uc[ 9]*tm[8]+uc[13]*tm[9];
    _4 =  uc[10]*tm[5]+uc[11]*tm[6]+uc[12]*tm[7]+uc[13]*tm[8]+uc[14]*tm[9];
    pc[2]  = tm[ 5]*_0 + tm[ 6]*_1 + tm[ 7]*_2 + tm[ 8]*_3 + tm[ 9]*_4;
    pc[4]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
    pc[7]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
    pc[11] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
    _0 = uc[ 0]*tm[10]+uc[ 1]*tm[11]+uc[ 3]*tm[12]+uc[ 6]*tm[13]+uc[10]*tm[14];
    _1 = uc[ 1]*tm[10]+uc[ 2]*tm[11]+uc[ 4]*tm[12]+uc[ 7]*tm[13]+uc[11]*tm[14];
    _2 = uc[ 3]*tm[10]+uc[ 4]*tm[11]+uc[ 5]*tm[12]+uc[ 8]*tm[13]+uc[12]*tm[14];
    _3 = uc[ 6]*tm[10]+uc[ 7]*tm[11]+uc[ 8]*tm[12]+uc[ 9]*tm[13]+uc[13]*tm[14];
    _4 = uc[10]*tm[10]+uc[11]*tm[11]+uc[12]*tm[12]+uc[13]*tm[13]+uc[14]*tm[14];
    pc[5]  = tm[10]*_0 + tm[11]*_1 + tm[12]*_2 + tm[13]*_3 + tm[14]*_4;
    pc[8]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
    pc[12] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
    _0 = uc[ 0]*tm[15]+uc[ 1]*tm[16]+uc[ 3]*tm[17]+uc[ 6]*tm[18]+uc[10]*tm[19];
    _1 = uc[ 1]*tm[15]+uc[ 2]*tm[16]+uc[ 4]*tm[17]+uc[ 7]*tm[18]+uc[11]*tm[19];
    _2 = uc[ 3]*tm[15]+uc[ 4]*tm[16]+uc[ 5]*tm[17]+uc[ 8]*tm[18]+uc[12]*tm[19];
    _3 = uc[ 6]*tm[15]+uc[ 7]*tm[16]+uc[ 8]*tm[17]+uc[ 9]*tm[18]+uc[13]*tm[19];
    _4 = uc[10]*tm[15]+uc[11]*tm[16]+uc[12]*tm[17]+uc[13]*tm[18]+uc[14]*tm[19];
    pc[9]  = tm[15]*_0 + tm[16]*_1 + tm[17]*_2 + tm[18]*_3 + tm[19]*_4;
    pc[13] = tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
    _0 = uc[ 0]*tm[20]+uc[1]*tm[21]+uc[3]*tm[22]+uc[6]*tm[23]+uc[10]*tm[24];
    _1 = uc[ 1]*tm[20]+uc[2]*tm[21]+uc[4]*tm[22]+uc[7]*tm[23]+uc[11]*tm[24];
    _2 = uc[ 3]*tm[20]+uc[4]*tm[21]+uc[5]*tm[22]+uc[8]*tm[23]+uc[12]*tm[24];
    _3 = uc[ 6]*tm[20]+uc[7]*tm[21]+uc[8]*tm[22]+uc[9]*tm[23]+uc[13]*tm[24];
    _4 = uc[10]*tm[20]+uc[11]*tm[21]+uc[12]*tm[22]+uc[13]*tm[23]+uc[14]*tm[24];
    pc[14]= tm[20]*_0 + tm[21]*_1 + tm[22]*_2 + tm[23]*_3 + tm[24]*_4;
  }

  static inline void update (
    const TrackVector& Xref,
    const TrackVector& H,
    const PRECISION& refResidual,
    const PRECISION& errorMeas,
    const TrackVector& ps,
    const TrackSymMatrix& pc,
    TrackVector& X,
    TrackSymMatrix& C,
    PRECISION& chi2
  ) {
    // The ugly code below makes the filter step about 20% faster
    // than SMatrix would do it.
    auto  res = refResidual +  H[0] * (Xref[0] - ps[0]) 
                            +  H[1] * (Xref[1] - ps[1])
                            +  H[2] * (Xref[2] - ps[2])
                            +  H[3] * (Xref[3] - ps[3]) 
                            +  H[4] * (Xref[4] - ps[4]);
    PRECISION CHT[5] = {
     pc[ 0]*H[0] + pc[ 1]*H[1] + pc[ 3]*H[2] + pc[ 6]*H[3] + pc[10]*H[4] ,
     pc[ 1]*H[0] + pc[ 2]*H[1] + pc[ 4]*H[2] + pc[ 7]*H[3] + pc[11]*H[4] ,
     pc[ 3]*H[0] + pc[ 4]*H[1] + pc[ 5]*H[2] + pc[ 8]*H[3] + pc[12]*H[4] ,
     pc[ 6]*H[0] + pc[ 7]*H[1] + pc[ 8]*H[2] + pc[ 9]*H[3] + pc[13]*H[4] ,
     pc[10]*H[0] + pc[11]*H[1] + pc[12]*H[2] + pc[13]*H[3] + pc[14]*H[4] 
    };
    auto errorResInv = 1.0 /
      (errorMeas * errorMeas
      + H[0]*CHT[0]
      + H[1]*CHT[1]
      + H[2]*CHT[2]
      + H[3]*CHT[3]
      + H[4]*CHT[4]);

    // update the state vector and cov matrix
    auto w = res * errorResInv;
    X[0] += CHT[0] * w;
    X[1] += CHT[1] * w;
    X[2] += CHT[2] * w;
    X[3] += CHT[3] * w;
    X[4] += CHT[4] * w;

    C[ 0] -= errorResInv * CHT[0] * CHT[0];
    C[ 1] -= errorResInv * CHT[1] * CHT[0];
    C[ 3] -= errorResInv * CHT[2] * CHT[0];
    C[ 6] -= errorResInv * CHT[3] * CHT[0];
    C[10] -= errorResInv * CHT[4] * CHT[0];

    C[ 2] -= errorResInv * CHT[1] * CHT[1];
    C[ 4] -= errorResInv * CHT[2] * CHT[1];
    C[ 7] -= errorResInv * CHT[3] * CHT[1];
    C[11] -= errorResInv * CHT[4] * CHT[1];

    C[ 5] -= errorResInv * CHT[2] * CHT[2];
    C[ 8] -= errorResInv * CHT[3] * CHT[2];
    C[12] -= errorResInv * CHT[4] * CHT[2];

    C[ 9] -= errorResInv * CHT[3] * CHT[3];
    C[13] -= errorResInv * CHT[4] * CHT[3];

    C[14] -= errorResInv * CHT[4] * CHT[4];

    chi2 = res * res * errorResInv;
  }

  static inline void average (
    const TrackVector& X1,
    const TrackSymMatrix& C1,
    const TrackVector& X2,
    const TrackSymMatrix& C2,
    TrackVector& X,
    TrackSymMatrix& C
  ) {
    // compute the inverse of the covariance (i.e. weight) of the difference: R=(C1+C2)
    TrackSymMatrixContiguous invRM;
    auto& invR = invRM.fArray;
    for (int i=0; i<15; ++i) {
      invR[i] = C1[i] + C2[i];
    }

    bool success = invRM.InvertChol();

    // compute the gain matrix

    // K <- C1*inverse(C1+C2) = C1*invR
    PRECISION K[25];
    K[ 0] = C1[ 0]*invR[ 0] + C1[ 1]*invR[ 1] + C1[ 3]*invR[ 3] + C1[ 6]*invR[ 6] + C1[10]*invR[10];
    K[ 1] = C1[ 0]*invR[ 1] + C1[ 1]*invR[ 2] + C1[ 3]*invR[ 4] + C1[ 6]*invR[ 7] + C1[10]*invR[11];
    K[ 2] = C1[ 0]*invR[ 3] + C1[ 1]*invR[ 4] + C1[ 3]*invR[ 5] + C1[ 6]*invR[ 8] + C1[10]*invR[12];
    K[ 3] = C1[ 0]*invR[ 6] + C1[ 1]*invR[ 7] + C1[ 3]*invR[ 8] + C1[ 6]*invR[ 9] + C1[10]*invR[13];
    K[ 4] = C1[ 0]*invR[10] + C1[ 1]*invR[11] + C1[ 3]*invR[12] + C1[ 6]*invR[13] + C1[10]*invR[14];

    K[ 5] = C1[ 1]*invR[ 0] + C1[ 2]*invR[ 1] + C1[ 4]*invR[ 3] + C1[ 7]*invR[ 6] + C1[11]*invR[10];
    K[ 6] = C1[ 1]*invR[ 1] + C1[ 2]*invR[ 2] + C1[ 4]*invR[ 4] + C1[ 7]*invR[ 7] + C1[11]*invR[11];
    K[ 7] = C1[ 1]*invR[ 3] + C1[ 2]*invR[ 4] + C1[ 4]*invR[ 5] + C1[ 7]*invR[ 8] + C1[11]*invR[12];
    K[ 8] = C1[ 1]*invR[ 6] + C1[ 2]*invR[ 7] + C1[ 4]*invR[ 8] + C1[ 7]*invR[ 9] + C1[11]*invR[13];
    K[ 9] = C1[ 1]*invR[10] + C1[ 2]*invR[11] + C1[ 4]*invR[12] + C1[ 7]*invR[13] + C1[11]*invR[14];

    K[10] = C1[ 3]*invR[ 0] + C1[ 4]*invR[ 1] + C1[ 5]*invR[ 3] + C1[ 8]*invR[ 6] + C1[12]*invR[10];
    K[11] = C1[ 3]*invR[ 1] + C1[ 4]*invR[ 2] + C1[ 5]*invR[ 4] + C1[ 8]*invR[ 7] + C1[12]*invR[11];
    K[12] = C1[ 3]*invR[ 3] + C1[ 4]*invR[ 4] + C1[ 5]*invR[ 5] + C1[ 8]*invR[ 8] + C1[12]*invR[12];
    K[13] = C1[ 3]*invR[ 6] + C1[ 4]*invR[ 7] + C1[ 5]*invR[ 8] + C1[ 8]*invR[ 9] + C1[12]*invR[13];
    K[14] = C1[ 3]*invR[10] + C1[ 4]*invR[11] + C1[ 5]*invR[12] + C1[ 8]*invR[13] + C1[12]*invR[14];

    K[15] = C1[ 6]*invR[ 0] + C1[ 7]*invR[ 1] + C1[ 8]*invR[ 3] + C1[ 9]*invR[ 6] + C1[13]*invR[10];
    K[16] = C1[ 6]*invR[ 1] + C1[ 7]*invR[ 2] + C1[ 8]*invR[ 4] + C1[ 9]*invR[ 7] + C1[13]*invR[11];
    K[17] = C1[ 6]*invR[ 3] + C1[ 7]*invR[ 4] + C1[ 8]*invR[ 5] + C1[ 9]*invR[ 8] + C1[13]*invR[12];
    K[18] = C1[ 6]*invR[ 6] + C1[ 7]*invR[ 7] + C1[ 8]*invR[ 8] + C1[ 9]*invR[ 9] + C1[13]*invR[13];
    K[19] = C1[ 6]*invR[10] + C1[ 7]*invR[11] + C1[ 8]*invR[12] + C1[ 9]*invR[13] + C1[13]*invR[14];

    K[20] = C1[10]*invR[ 0] + C1[11]*invR[ 1] + C1[12]*invR[ 3] + C1[13]*invR[ 6] + C1[14]*invR[10];
    K[21] = C1[10]*invR[ 1] + C1[11]*invR[ 2] + C1[12]*invR[ 4] + C1[13]*invR[ 7] + C1[14]*invR[11];
    K[22] = C1[10]*invR[ 3] + C1[11]*invR[ 4] + C1[12]*invR[ 5] + C1[13]*invR[ 8] + C1[14]*invR[12];
    K[23] = C1[10]*invR[ 6] + C1[11]*invR[ 7] + C1[12]*invR[ 8] + C1[13]*invR[ 9] + C1[14]*invR[13];
    K[24] = C1[10]*invR[10] + C1[11]*invR[11] + C1[12]*invR[12] + C1[13]*invR[13] + C1[14]*invR[14];

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    PRECISION d[5] { X2[0]-X1[0], X2[1]-X1[1], X2[2]-X1[2], X2[3]-X1[3], X2[4]-X1[4] };
    X[0] = X1[0] + K[ 0]*d[0] + K[ 1]*d[1] + K[ 2]*d[2] + K[ 3]*d[3] + K[ 4]*d[4];
    X[1] = X1[1] + K[ 5]*d[0] + K[ 6]*d[1] + K[ 7]*d[2] + K[ 8]*d[3] + K[ 9]*d[4];
    X[2] = X1[2] + K[10]*d[0] + K[11]*d[1] + K[12]*d[2] + K[13]*d[3] + K[14]*d[4];
    X[3] = X1[3] + K[15]*d[0] + K[16]*d[1] + K[17]*d[2] + K[18]*d[3] + K[19]*d[4];
    X[4] = X1[4] + K[20]*d[0] + K[21]*d[1] + K[22]*d[2] + K[23]*d[3] + K[24]*d[4];

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    C[ 0] = K[ 0]*C2[ 0] + K[ 1]*C2[ 1] + K[ 2]*C2[ 3] + K[ 3]*C2[ 6] + K[ 4]*C2[10];
    C[ 1] = K[ 5]*C2[ 0] + K[ 6]*C2[ 1] + K[ 7]*C2[ 3] + K[ 8]*C2[ 6] + K[ 9]*C2[10];
    C[ 3] = K[10]*C2[ 0] + K[11]*C2[ 1] + K[12]*C2[ 3] + K[13]*C2[ 6] + K[14]*C2[10];
    C[ 6] = K[15]*C2[ 0] + K[16]*C2[ 1] + K[17]*C2[ 3] + K[18]*C2[ 6] + K[19]*C2[10];
    C[10] = K[20]*C2[ 0] + K[21]*C2[ 1] + K[22]*C2[ 3] + K[23]*C2[ 6] + K[24]*C2[10]; 

    C[ 2] = K[ 5]*C2[ 1] + K[ 6]*C2[ 2] + K[ 7]*C2[ 4] + K[ 8]*C2[ 7] + K[ 9]*C2[11];
    C[ 4] = K[10]*C2[ 1] + K[11]*C2[ 2] + K[12]*C2[ 4] + K[13]*C2[ 7] + K[14]*C2[11];
    C[ 7] = K[15]*C2[ 1] + K[16]*C2[ 2] + K[17]*C2[ 4] + K[18]*C2[ 7] + K[19]*C2[11];
    C[11] = K[20]*C2[ 1] + K[21]*C2[ 2] + K[22]*C2[ 4] + K[23]*C2[ 7] + K[24]*C2[11]; 

    C[ 5] = K[10]*C2[ 3] + K[11]*C2[ 4] + K[12]*C2[ 5] + K[13]*C2[ 8] + K[14]*C2[12];
    C[ 8] = K[15]*C2[ 3] + K[16]*C2[ 4] + K[17]*C2[ 5] + K[18]*C2[ 8] + K[19]*C2[12];
    C[12] = K[20]*C2[ 3] + K[21]*C2[ 4] + K[22]*C2[ 5] + K[23]*C2[ 8] + K[24]*C2[12]; 

    C[ 9] = K[15]*C2[ 6] + K[16]*C2[ 7] + K[17]*C2[ 8] + K[18]*C2[ 9] + K[19]*C2[13];
    C[13] = K[20]*C2[ 6] + K[21]*C2[ 7] + K[22]*C2[ 8] + K[23]*C2[ 9] + K[24]*C2[13]; 

    C[14] = K[20]*C2[10] + K[21]*C2[11] + K[22]*C2[12] + K[23]*C2[13] + K[24]*C2[14]; 
  }
};

}

}
