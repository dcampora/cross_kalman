#pragma once

#include "MemViewMatrix.h"

namespace VectorFit {

// Operators accepted for templated get method
namespace Op {
  // Data container
  class NodeParameters;
  class Backward;
  class Forward;
  class Smooth;

  // Data type
  // NodeParameters
  class ReferenceVector;
  class ProjectionMatrix;
  class TransportVector;
  class NoiseMatrix;
  class ReferenceResidual;
  class ErrMeasure;

  // Forward, Backward, Smooth
  class StateVector;
  class Covariance;
  
  // Forward, Backward
  class Chi2;
  
  // Smooth
  class TransportMatrix;
  class Residual;
  class ErrResidual;
}

// Data views

namespace Mem {

namespace View {

struct NodeParameters {
  constexpr static size_t size () {
    return 32;
  }
  PRECISION* m_basePointer = 0x0;
  TrackVector m_referenceVector;
  TrackVector m_projectionMatrix;
  TrackSymMatrix m_noiseMatrix;
  TrackVector m_transportVector;
  PRECISION* m_referenceResidual;
  PRECISION* m_errorMeasure;

  NodeParameters () = default;
  NodeParameters (const NodeParameters& copy) = default;
  NodeParameters (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_referenceVector = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_projectionMatrix = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_noiseMatrix = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_transportVector = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_referenceResidual = p; p += VECTOR_WIDTH;
    m_errorMeasure = p;
  }

  void setBasePointer (const NodeParameters& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const NodeParameters& s) {
    m_referenceVector.copy(s.m_referenceVector);
    m_projectionMatrix.copy(s.m_projectionMatrix);
    m_noiseMatrix.copy(s.m_noiseMatrix);
    m_transportVector.copy(s.m_transportVector);
    *m_referenceResidual = *s.m_referenceResidual;
    *m_errorMeasure = *s.m_errorMeasure;
  }
};

struct SmoothState {
  constexpr static size_t size () {
    return 22;
  }
  PRECISION* m_basePointer = 0x0;
  TrackVector m_state;
  TrackSymMatrix m_covariance;
  PRECISION* m_residual;
  PRECISION* m_errResidual;

  SmoothState () = default;
  SmoothState (const SmoothState& copy) = default;
  SmoothState (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_state = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_covariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_residual = p; p += VECTOR_WIDTH;
    m_errResidual = p;
  }

  void setBasePointer (const SmoothState& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const SmoothState& s) {
    m_state.copy(s.m_state);
    m_covariance.copy(s.m_covariance);
    *m_residual = *s.m_residual;
    *m_errResidual = *s.m_errResidual;
  }
};

struct State {
  constexpr static size_t size () {
    return 21;
  }
  PRECISION* m_basePointer = 0x0;
  TrackVector m_updatedState;
  TrackSymMatrix m_updatedCovariance;
  PRECISION* m_chi2;

  State () = default;
  State (const State& copy) = default;
  State (PRECISION* m_basePointer) {
    setBasePointer(m_basePointer);
  }

  void setBasePointer (PRECISION* p) {
    m_basePointer = p;
    m_updatedState = TrackVector(p); p += 5 * VECTOR_WIDTH;
    m_updatedCovariance = TrackSymMatrix(p); p += 15 * VECTOR_WIDTH;
    m_chi2 = p;
  }

  void setBasePointer (const State& s) {
    setBasePointer(s.m_basePointer);
  }

  void copy (const State& s) {
    m_updatedState.copy(s.m_updatedState);
    m_updatedCovariance.copy(s.m_updatedCovariance);
    *m_chi2 = *(s.m_chi2);
  }
};

}

}

}
