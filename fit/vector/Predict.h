#pragma once

#include "../Types.h"
#include "../ArrayGen.h"
#ifdef USE_VCL
  #include "vcl/Math.h"
#elif defined(USE_UMESIMD)
  #include "umesimd/Math.h"
#endif

namespace VectorFit {

namespace Vector {

template<class T>
struct predict {};

template<>
struct predict<Op::Forward> {
  template<unsigned W>
  static inline void op (
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64_const nm,
    fp_ptr_64_const tv,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    Math<Op::Forward>::predictState<W> (
      tm,
      tv,
      us,
      ps
    );

    Math<Op::Forward>::predictCovariance<W> (
      tm,
      nm,
      uc,
      pc
    );
  }
};

template<>
struct predict<Op::Backward> {
  template<unsigned W>
  static inline void op (
    const std::array<Sch::Item, W>& n,
    const std::vector<VectorFit::Track>& t,
    fp_ptr_64_const tm,
    fp_ptr_64_const us,
    fp_ptr_64_const uc,
    fp_ptr_64 ps,
    fp_ptr_64 pc
  ) {
    _aligned const std::array<PRECISION, 5*W> tv = ArrayGenCommon::getPreviousTransportVector(n, t);
    _aligned std::array<PRECISION, 15*W> nm = ArrayGenCommon::getPreviousNoiseMatrix(n, t);
    fp_ptr_64_const tv_p = tv.data();
    fp_ptr_64 nm_p = nm.data();

    Math<Op::Backward>::predictState<W> (
      tm,
      tv_p,
      us,
      ps
    );

    Math<Op::Backward>::predictCovariance<W> (
      tm,
      nm_p,
      uc,
      pc
    );
  }
};

}

}
