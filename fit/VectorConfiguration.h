#pragma once

#define TO_STRING(s) WRAPPED_TO_STRING(s)
#define WRAPPED_TO_STRING(s) #s

// Set compiling values
#ifdef SP

#define PRECISION float
#define SIZE_OF_PRECISION 4

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__) || defined(__AVX512__)
  #define VECTOR_WIDTH 16u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 8u
#elif defined(__SSE__) || defined(__aarch64__) || defined(__ALTIVEC__)
  #define VECTOR_WIDTH 4u
#else
  #define VECTOR_WIDTH 1u
#endif

#else

#define PRECISION double
#define SIZE_OF_PRECISION 8

#ifdef STATIC_VECTOR_WIDTH
  #define VECTOR_WIDTH STATIC_VECTOR_WIDTH
#elif defined(__AVX512F__) || defined(__AVX512__)
  #define VECTOR_WIDTH 8u
#elif defined(__AVX__)
  #define VECTOR_WIDTH 4u
#elif defined(__SSE__) || defined(__aarch64__) || defined(__ALTIVEC__)
  #define VECTOR_WIDTH 2u
#else
  #define VECTOR_WIDTH 1u
#endif

#endif

// Some align definitions
#define ALIGNMENT sizeof(PRECISION) * VECTOR_WIDTH
#define _aligned alignas(ALIGNMENT)
#ifdef __INTEL_COMPILER
#define _type_aligned alignas(ALIGNMENT)
#else
#define _type_aligned __attribute__((aligned(ALIGNMENT)))
#endif

typedef PRECISION * const __restrict__ fp_ptr_64;
typedef const PRECISION * const __restrict__ fp_ptr_64_const;

template <unsigned W> struct Vectype { using type = PRECISION; using booltype = bool; };

#ifdef USE_VCL
  #include "vector/vcl/VectorConfiguration.h"
#elif defined(USE_UMESIMD)
  #include "vector/umesimd/VectorConfiguration.h"
#endif

