#pragma once

#include "CholeskyDecomp.h"
#include <array>

struct TrackMatrix {
  double fArray[25];

  TrackMatrix () = default;
  TrackMatrix (const std::array<double, 25>& v) {
    for (unsigned i=0; i<25; ++i) {
      fArray[i] = v[i];
    }
  }
  TrackMatrix (const std::array<float, 25>& v) {
    for (unsigned i=0; i<25; ++i) {
      fArray[i] = v[i];
    }
  }

  double& operator()(const int x, const int y) {return fArray[x*5+y]; }
  double operator()(const int x, const int y) const {return fArray[x*5+y]; }
  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}
};

struct TrackVector {
  double fArray[5];

  TrackVector () = default;
  TrackVector (double a0, double a1, double a2, double a3, double a4) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
  }

  double& operator()(const int x) {return fArray[x]; }
  double operator()(const int x) const {return fArray[x]; }
  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}
};

struct TrackSymMatrix {
  double fArray[15];

  TrackSymMatrix () = default;
  TrackSymMatrix (const TrackSymMatrix&) = default;
  TrackSymMatrix (const double& a0, const double& a1, const double& a2, const double& a3, const double& a4,
    const double& a5, const double& a6, const double& a7, const double& a8, const double& a9,
    const double& a10, const double& a11, const double& a12, const double& a13, const double& a14) {
    fArray[0] = a0;
    fArray[1] = a1;
    fArray[2] = a2;
    fArray[3] = a3;
    fArray[4] = a4;
    fArray[5] = a5;
    fArray[6] = a6;
    fArray[7] = a7;
    fArray[8] = a8;
    fArray[9] = a9;
    fArray[10] = a10;
    fArray[11] = a11;
    fArray[12] = a12;
    fArray[13] = a13;
    fArray[14] = a14;
  }

  bool InvertChol() {
    ROOT::Math::CholeskyDecomp<double, 5> decomp(fArray);
    return decomp.Invert(fArray);
  }

  double& operator() (const int row, const int col) {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double operator()(const int row, const int col) const {
    return row>col ?
      fArray[row*(row+1)/2 + col] :
      fArray[col*(col+1)/2 + row];
  }

  double& operator[](const int x) {return fArray[x];}
  double operator[](const int x) const {return fArray[x];}
};

TrackVector operator*(const TrackMatrix& mA, const TrackVector& mB);
TrackVector operator+(const TrackVector& mA, const TrackVector& mB);
TrackVector operator-(const TrackVector& mA, const TrackVector& mB);
TrackSymMatrix operator+(const TrackSymMatrix& mA, const TrackSymMatrix& mB);
void operator+=(TrackSymMatrix& mA, const TrackSymMatrix& mB);
double operator*(const TrackVector& mA, const TrackVector& mB);

typedef TrackVector TrackProjectionMatrix;
