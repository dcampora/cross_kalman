#pragma once

#include "../gaudi/Types.h"
#include "../fit/Types.h"
#include <cassert>
#include <fstream>
#include <vector>
#include <iostream>
#include <string>

/**
 * @brief      Instance of the kalman filter
 */
struct Instance {
  std::vector<std::vector<FitNode>> typecastedTracks;
  std::vector<std::vector<FitNode>> expectedResult;
  std::vector<VectorFit::Track> tracks;
};

/**
 * @brief      Some useful information about the events
 */
struct InstanceInfo {
  unsigned numberOfNodes, numberOfReferenceNodes, numberOfHitNodes;
  InstanceInfo () : numberOfNodes(0), numberOfReferenceNodes(0), numberOfHitNodes(0) {}
};

std::vector<Instance> translateFileIntoEvent (
  const std::vector<uint8_t>& fileContents,
  VectorFit::Mem::Store& store,
  const bool& checkResults,
  const unsigned& minTrackBatchSize
);

void printInfo (const std::vector<Instance>& instances);
