#include "Instance.h"

std::vector<Instance> translateFileIntoEvent (
  const std::vector<uint8_t>& fileContents,
  VectorFit::Mem::Store& store,
  const bool& checkResults,
  const unsigned& minTrackBatchSize
) {
  std::vector<Instance> t;
  uint8_t* p = const_cast<uint8_t*>(fileContents.data());

  while (p < fileContents.data() + fileContents.size()) {
    Instance instance;

    p += sizeof(uint8_t);
    const bool doBismooth = (bool) *p; p += sizeof(uint8_t);
    const int numberOfTracks = *((int*) p); p += sizeof(uint32_t);

    instance.typecastedTracks.resize(numberOfTracks);
    if (checkResults) {
      instance.expectedResult.resize(numberOfTracks);
    }

    for (int i=0; i<numberOfTracks; ++i) {
      int memblockSize = *((int*) p); p += sizeof(uint32_t);
      
      instance.typecastedTracks[i].resize(memblockSize);
      std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.typecastedTracks[i].data()));
      p += memblockSize * sizeof(FitNode);

      if (checkResults) {
        instance.expectedResult[i].resize(memblockSize);
        std::copy(p, p + memblockSize * sizeof(FitNode), ((uint8_t*) instance.expectedResult[i].data()));
      }
      p += memblockSize * sizeof(FitNode);
    }

    if (instance.typecastedTracks.size() > 0 && instance.typecastedTracks[0].size() > 0) {
      auto doFit = [&] (const int& direction) {
        for (const auto& t : instance.typecastedTracks) {
          for (const auto& n : t) {
            if (n.requirePredictedState(direction)) {
              return true;
            }
          }
        }
        return false;
      };

      const bool doForwardFit = doFit(Forward);
      const bool doBackwardFit = doForwardFit || doFit(Backward);

      auto checkOutlier = [&] {
        for (const auto& n : instance.typecastedTracks[0]) {
          if (!n.requirePredictedState(Forward) || !n.requirePredictedState(Forward)) {
            return true;
          }
        }
        return false;
      };
      const bool isOutlier = (doForwardFit || doBackwardFit) && checkOutlier();

      if (!isOutlier &&
        doForwardFit &&
        doBackwardFit &&
        doBismooth &&
        instance.typecastedTracks.size() >= minTrackBatchSize
      ) {
        // Generate new tracks
        for (unsigned i=0; i<instance.typecastedTracks.size(); ++i) {
          instance.tracks.push_back(VectorFit::Track(instance.typecastedTracks[i], i, store));
        }

        t.push_back(instance);
      }
    }
  }

  assert(p == fileContents.data() + fileContents.size());
  return t;
}

void printInfo(const std::vector<Instance>& t) {
  std::vector<InstanceInfo> instancesInfo;

  for (const auto& instance : t) {
    InstanceInfo info;
    for (const auto& track : instance.tracks) {
      info.numberOfNodes += track.m_nodes.size();
      for (const auto& node : track.m_nodes) {
        info.numberOfHitNodes += (node.m_type == HitOnTrack);
        info.numberOfReferenceNodes += (node.m_type == Reference);
      }
    }
    instancesInfo.push_back(info);
  }

  unsigned totalNumberOfNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfNodes; return acc; } ();
  unsigned totalNumberOfRefNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfReferenceNodes; return acc; } ();
  unsigned totalNumberOfHitNodes = [&] {unsigned acc = 0; for (const auto& info : instancesInfo) acc += info.numberOfHitNodes; return acc; } ();

  std::cout << " number of instances: " << t.size() << std::endl;
  std::cout << " total number of nodes: " << totalNumberOfNodes
    << " (" << totalNumberOfHitNodes << " hits, " << totalNumberOfRefNodes << " references)" << std::endl;

  std::cout << " #tracks in each instance: ";
  for (unsigned i=0; i<t.size(); ++i) {
    std::cout << t[i].tracks.size();
    if (i != t.size() - 1) { std::cout << ", "; }
  }
  std::cout << std::endl;

  std::cout << " #nodes in each instance: ";
  for (unsigned i=0; i<instancesInfo.size(); ++i) {
    std::cout << instancesInfo[i].numberOfNodes;
    if (i != t.size() - 1) { std::cout << ", "; }
  }
  std::cout << std::endl << std::endl;
}
