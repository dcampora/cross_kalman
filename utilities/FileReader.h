#pragma once

#include <iostream>
#include <string>
#include <cstring>
#include <exception>
#include <fstream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cstdio>
#include <dirent.h>

/**
 * Generic StrException launcher
 */
struct StrException : public std::exception
{
  std::string s;
  StrException(std::string ss) : s(ss) {}
  ~StrException() throw () {} // Updated
  const char* what() const throw() { return s.c_str(); }
};

class FileReader {
private:

  /**
   * Checks file existence
   * @param  name 
   * @return      
   */
  bool fileExists (const std::string& name) {
    if (FILE *file = fopen(name.c_str(), "r")) {
      fclose(file);
      return true;
    } else {
      return false;
    }   
  }

public:
  /**
   * Reads some data from an input file, following the
   * specified format of choice
   *
   * Format expected in file:
   *
   * int funcNameLen
   * char* funcName
   * int dataSize
   * char* data
   */
  void readFileIntoVector (std::string filename, std::vector<uint8_t>& output){
    // Check if file exists
    if (!fileExists(filename)) {
      throw StrException("Error: File " + filename + " does not exist");
    }

    std::ifstream file (filename.c_str(), std::ios::in | std::ios::binary | std::ios::ate);

    // Reserve space, read file
    size_t filesize = (size_t) file.tellg();

    output.resize (filesize);
    
    file.seekg (0, std::ios::beg);
    file.read ((char*) output.data(), filesize);
    file.close ();
  }

  std::vector<std::string> getFilenamesFromFolder (
    const std::string& folderName,
    const unsigned int requestedInstances = 0,
    const std::string& extension = "bin"
  ) {
    std::vector<std::string> dataFilesInFolder;

    // Directory listing in C
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (folderName.c_str())) != nullptr) {
      while ((ent = readdir (dir)) != nullptr) {
        std::string filename = std::string(ent->d_name);
        if (filename.substr(filename.find_last_of(".") + 1) == extension) {
          dataFilesInFolder.push_back(filename);
        }
      }
      closedir (dir);
    }
    if (dir == nullptr || dataFilesInFolder.empty()) {
      throw StrException("Directory " + folderName + " either could not be opened, or is empty");
    }

    std::sort(dataFilesInFolder.begin(), dataFilesInFolder.end());

    // Process in round robin the files required
    std::cout << "Directory contains " << dataFilesInFolder.size() << " elements." << std::endl;

    if (requestedInstances > 0 && requestedInstances < dataFilesInFolder.size()) {
      dataFilesInFolder.resize(requestedInstances);
    }
    
    return dataFilesInFolder;
  }

  void readFilesFromFolder (
    std::vector<std::vector<uint8_t>>& filecontents,
    const std::string& folderName,
    const unsigned int requestedInstances = 0,
    const std::string& extension = "bin"
  ) {
    // Cleaning and reserving filecontents
    filecontents.clear();

    std::vector<std::string> dataFilesInFolder;

    // Directory listing in C
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir (folderName.c_str())) != nullptr) {
      while ((ent = readdir (dir)) != nullptr) {
        std::string filename = std::string(ent->d_name);
        if (filename.substr(filename.find_last_of(".") + 1) == extension) {
          dataFilesInFolder.push_back(filename);
        }
      }
      closedir (dir);
    }
    if (dir == nullptr || dataFilesInFolder.empty()) {
      throw StrException("Directory " + folderName + " either could not be opened, or is empty");
    }

    std::sort(dataFilesInFolder.begin(), dataFilesInFolder.end());

    // Process in round robin the files required
    std::cout << "Directory contains " << dataFilesInFolder.size() << " elements." << std::endl
      << "Opening requested files..." << std::endl;
    
    const unsigned int instances = requestedInstances > 0 ? requestedInstances : dataFilesInFolder.size();
    filecontents.resize(instances);

    for (unsigned int i=0; i<instances; ++i) {
      unsigned int filenumber = i % dataFilesInFolder.size();
      readFileIntoVector(folderName + "/" + dataFilesInFolder[filenumber], filecontents[i]);
      std::cout << "." << std::flush;
    }
    std::cout << std::endl;
  }
};
