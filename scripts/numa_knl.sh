#!/bin/bash

numactl -m 2 taskset -c 0-31,64-95,128-159,192-223 ./cross_kalman -w 5000 -n 20000 > p0_run.out &
numactl -m 3 taskset -c 32-63,96-127,160-191,224-255 ./cross_kalman -w 5000 -n 20000 > p1_run.out &

