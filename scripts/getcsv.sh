#!/bin/bash

DIR=$1
OUTFILE=$2

echo number of processors, throughput >> $OUTFILE
for i in `ls -rt $DIR | grep out`
do
  echo $i
  PROC_NUM=`echo $i | sed -e "s/\_[0-9]*\.out//g"`
  THROUGHPUT=`cat $DIR/$i | grep "Fit throughput" | awk '{ print $6; }'`
  echo $PROC_NUM, $THROUGHPUT >> $OUTFILE
done

