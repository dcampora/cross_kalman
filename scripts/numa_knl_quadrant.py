#!/usr/bin/python

import sys

numa_domains = (sys.argv[1]).split("_")
selected_domain = int(sys.argv[2])
num_processors = int(sys.argv[3])

import re
domain, mem = numa_domains[selected_domain], selected_domain

# Create list of processors from domain string
processor_list = []
for m in re.finditer("(\d+)\-(\d+)\,?", domain):
  for j in range(int(m.group(1)), int(m.group(2)) + 1):
    processor_list.append(j)

# Safe guard
if num_processors > len(processor_list):
  num_processors = len(processor_list)

sys.stdout.write("numactl -m 1 -C ")
for i in range(0, num_processors):
  sys.stdout.write(str(processor_list[i]))
  if i != num_processors - 1:
    sys.stdout.write(",")
